document.addEventListener('DOMContentLoaded', function() {
    fetchPersons();

    document.getElementById('personForm').addEventListener('submit', function(event) {
        event.preventDefault();
        const person = {
            id: document.getElementById('id').value,
            firstName: document.getElementById('firstName').value,
            lastName: document.getElementById('lastName').value,
            age: document.getElementById('age').value
        };
        const url = person.id ? 'api/v1/person' : 'api/v1/person';
        const method = person.id ? 'PATCH' : 'POST';

        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(person)
        })
            .then(response => response.json())
            .then(() => {
                document.getElementById('id').value = '';
                document.getElementById('firstName').value = '';
                document.getElementById('lastName').value = '';
                document.getElementById('age').value = '';
                fetchPersons();
            });
    });
});

function fetchPersons() {
    fetch('/158988/api/v1/persons')
        .then(response => response.json())
        .then(data => {
            const personsTable = document.getElementById('personsTable');
            personsTable.innerHTML = '';
            data.forEach(person => {
                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${person.id}</td>
                    <td>${person.firstName}</td>
                    <td>${person.lastName}</td>
                    <td>${person.age}</td>
                    <td>
                        <button onclick="editPerson('${person.id}')">Edit</button>
                        <button onclick="deletePerson('${person.id}')">Delete</button>
                    </td>
                `;
                personsTable.appendChild(row);
            });
        });
}

function editPerson(id) {
    fetch(`/158988/api/v1/person/${id}`)
        .then(response => response.json())
        .then(data => {
            document.getElementById('id').value = data.id;
            document.getElementById('firstName').value = data.firstName;
            document.getElementById('lastName').value = data.lastName;
            document.getElementById('age').value = data.age;
        });
}

function deletePerson(id) {
    fetch(`/158988/api/v1/person/${id}`, {
        method: 'DELETE'
    })
        .then(() => {
            fetchPersons();
        });
}
