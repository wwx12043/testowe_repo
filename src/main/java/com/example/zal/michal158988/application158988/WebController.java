package com.example.zal.michal158988.application158988;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/158988")
public class WebController {

    @GetMapping("/app")
    public String app() {
        return "index";
    }
}
