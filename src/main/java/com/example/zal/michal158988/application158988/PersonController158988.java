package com.example.zal.michal158988.application158988;

import com.example.zal.michal158988.domain158988.Person158988;
import com.example.zal.michal158988.domain158988.PersonManagerFacade158988;
import com.example.zal.michal158988.domain158988.dto158988.PersonDto158988;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/158988")
public class PersonController158988 {

    private final PersonManagerFacade158988 personManagerFacade;

    public PersonController158988(PersonManagerFacade158988 personManagerFacade) {
        this.personManagerFacade = personManagerFacade;
    }

    @GetMapping(value = "/api/v1/person/{id}")
    @ApiOperation(value = "Get Person by ID", notes = "Zwróć osobę po jej/jego ID.")
    public ResponseEntity<PersonDto158988> getPerson(@PathVariable String id) {
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(personManagerFacade.getPersonById(id));
    }

    @PostMapping( value ="/api/v1/person")
    @ApiOperation(value = "Create Person", notes = "Dodaj nową osobę.")
    public ResponseEntity<PersonDto158988> createPerson(@RequestBody Person158988 person) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(personManagerFacade.createPerson(person));
    }

    @PatchMapping(value = "/api/v1/person")
    @ApiOperation(value = "Update Person", notes = "Zaktualizuj już istniejącą osobę.")
    public ResponseEntity<PersonDto158988> updatePerson(@RequestBody Person158988 person) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(personManagerFacade.updatePerson(person));
    }

    @DeleteMapping(value ="/api/v1/person/{id}")
    @ApiOperation(value = "Delete Person by ID", notes = "Usuń osobę po jej ID.")
    public ResponseEntity<Void> deletePerson(@PathVariable String id) {
        personManagerFacade.deletePerson(id);
        return ResponseEntity.noContent().build();
    }


    @GetMapping(value = "/api/v1/persons")
    @ApiOperation(value = "Get All Persons", notes = "Zwróć wszystkie osoby.")
    public ResponseEntity<List<PersonDto158988>> getAllPersons() {
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(personManagerFacade.getAllPersons());
    }

    @GetMapping("/api/v1/persons/csv")
    @ApiOperation(value = "Get All Persons from CSV", notes = "Zwróć wszystkie osoby zapisane do pliku CSV.")
    public ResponseEntity<List<PersonDto158988>> getAllPersonsFromCsv() {
        List<PersonDto158988> persons = personManagerFacade.getAllPersonsFromCsv();
        return ResponseEntity.ok(persons);
    }

    @GetMapping("/api/v1/persons/download-csv")
    @ApiOperation(value = "Download CSV", notes = "Pobierz plik CSV.")
    public ResponseEntity<byte[]> downloadCsv() {
        byte[] data = personManagerFacade.getCsvFileContent();
        if (data.length == 0) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "plikTabelaryczny158988.csv");

        return new ResponseEntity<>(data, headers, HttpStatus.OK);
    }


}
