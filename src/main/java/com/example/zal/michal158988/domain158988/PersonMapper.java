package com.example.zal.michal158988.domain158988;

import com.example.zal.michal158988.domain158988.dto158988.PersonDto158988;

class PersonMapper {

    private PersonMapper(){}

    static PersonDto158988 mapFromPerson(Person158988 person) {
        return PersonDto158988.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .age(person.getAge())
                .build();
    }
}
