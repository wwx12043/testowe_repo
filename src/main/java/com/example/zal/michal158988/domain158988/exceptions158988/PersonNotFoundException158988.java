package com.example.zal.michal158988.domain158988.exceptions158988;

public class PersonNotFoundException158988 extends RuntimeException {
    public PersonNotFoundException158988(String message) {
        super(message);
    }
}
