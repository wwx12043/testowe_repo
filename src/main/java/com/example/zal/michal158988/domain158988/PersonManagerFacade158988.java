package com.example.zal.michal158988.domain158988;

import com.example.zal.michal158988.domain158988.dto158988.PersonDto158988;
import com.example.zal.michal158988.domain158988.exceptions158988.PersonNotFoundException158988;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class PersonManagerFacade158988 {

    @Qualifier("inMemoryDbRepository158988")
    private final PersonManagerRepository158988 repository;
    private final FileManager158988 manager = new FileManager158988();

    public PersonDto158988 createPerson(Person158988 person) {
        String personId = UUID.randomUUID().toString();
        Person158988 savedPerson = repository.save(new Person158988(personId, person.getFirstName(), person.getLastName(), person.getAge()));
        manager.writeToTxt(savedPerson);
        manager.writeToCsv(savedPerson);
        return PersonMapper.mapFromPerson(savedPerson);
    }

    public PersonDto158988 getPersonById(String id) {
        Person158988 person = repository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException158988("Person with ID " + id + " not found"));
        return PersonMapper.mapFromPerson(person);
    }

    public List<PersonDto158988> getAllPersons() {
        List<Person158988> persons = repository.findAllPersons();
        return persons.stream()
                .map(PersonMapper::mapFromPerson)
                .collect(Collectors.toList());
    }

    public List<PersonDto158988> getAllPersonsFromCsv() {
        return manager.readFromCsv();
    }

    public byte[] getCsvFileContent() {
        return manager.getCsvFileContent();
    }

    public void deletePerson(String id) {
        repository.delete(id);
    }

    public PersonDto158988 updatePerson(Person158988 person) {
        Person158988 updated = repository.update(person);
        return PersonMapper.mapFromPerson(updated);
    }

}